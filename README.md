# Section 3 Android Marathon

Task "Conventions"

1. [Comparison](https://gitlab.com/chd-k/section-3-android-marathon/-/tree/main/Comparison)
1. [Ranges](https://gitlab.com/chd-k/section-3-android-marathon/-/tree/main/Ranges)
1. [For loop](https://gitlab.com/chd-k/section-3-android-marathon/-/tree/main/For%20loop)
1. [Operators overloading](https://gitlab.com/chd-k/section-3-android-marathon/-/tree/main/Operators%20overloading)
1. [Invoke](https://gitlab.com/chd-k/section-3-android-marathon/-/tree/main/Invoke)
